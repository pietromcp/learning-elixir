defmodule MyList do
	def sum([]) do
		0
	end
	def sum([h | t]) do
		h + sum(t)
	end

	defp sum_loop(acc, []), do: acc
	defp sum_loop(acc, [h|t]) do
		sum_loop(acc + h, t)
	end

	def sum_tr(l), do: sum_loop(0, l)

	defp prod_loop(acc, []), do: acc
	defp prod_loop(acc, [h|t]), do: prod_loop(acc * h, t)

	def prod_tr(l), do: prod_loop(1, l)

	def prod_r(l), do: reduce(1, l, fn(acc, curr) -> acc * curr end)
	def sum_r(l), do: reduce(0, l, fn(acc, curr) -> acc + curr end)

	def reduce(seed, [], _), do: seed
	def reduce(seed, [h|t], f), do: reduce(f.(seed, h), t, f)

	def reverse([]), do: []
	def reverse([h|t]), do: reverse(t) ++ [h]

	defp reverse_loop(acc, []), do: acc
	defp reverse_loop(acc, [h|t]), do: reverse_loop([h] ++ acc, t)

	def reverse_tr(l), do: reverse_loop([], l)

	def reverse_red(l), do: reduce([], l, fn(acc, curr) -> [curr] ++ acc end)
end
