defmodule MyTuple do
  def first(t) do
    elem(t, 0)
  end
end
