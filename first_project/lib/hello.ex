defmodule Hello do
	def sayHello(), do: sayHello("World")
	def sayHello(to), do: "Hello, #{to}!"
end