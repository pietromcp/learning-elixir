defmodule MyListTest do
  use ExUnit.Case
  doctest MyList

  test "sum of empty list items is 0" do
    assert MyList.sum([]) == 0
  end

  test "sum of three ints" do
    assert MyList.sum([1, 2, 3]) == 6
  end

  test "sum of six ints" do
    assert MyList.sum([1, 2, 3, 4, 5, 6]) == 21
  end

  test "sum of six ints using recursion" do
    assert MyList.sum_r([1, 2, 3, 4, 5, 6]) == 21
  end

  test "sum of six ints using tail recursion" do
    assert MyList.sum_tr([1, 2, 3, 4, 5, 6]) == 21
  end

  test "multiply six ints using tail recursion" do
    assert MyList.prod_tr([2, 2, 2, 2, 2, 2]) == 64
  end

  test "multiply six ints" do
    assert MyList.prod_r([2, 2, 2, 2, 2, 2]) == 64
  end

  test "reverse empty list" do
    assert MyList.reverse([]) == []
  end

  test "reverse not empty list" do
    assert MyList.reverse([1, 2, 3, 4, 5, 6]) == [6, 5, 4, 3, 2, 1]
  end

  test "reverse_tr empty list" do
    assert MyList.reverse_tr([]) == []
  end

  test "reverse_tr not empty list" do
    assert MyList.reverse_tr([1, 2, 3, 4, 5, 6]) == [6, 5, 4, 3, 2, 1]
  end

  test "reverse_red empty list" do
    assert MyList.reverse_red([]) == []
  end

  test "reverse_red not empty list" do
    assert MyList.reverse_red([1, 2, 3, 4, 5, 6]) == [6, 5, 4, 3, 2, 1]
  end
end
