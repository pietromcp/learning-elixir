defmodule HelloTest do
  use ExUnit.Case
  doctest Hello

  test "greets the world" do
    assert Hello.sayHello() == "Hello, World!"
  end

  test "greets Pietro" do
    assert Hello.sayHello("Pietro") == "Hello, Pietro!"
  end
end
