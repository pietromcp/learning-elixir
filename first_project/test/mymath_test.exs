defmodule MyMathTest do
  use ExUnit.Case
  doctest MyMath

  test "fact of 0 is 1" do
    assert MyMath.fact(0) == 1
  end

  test "fact of 1 is 1" do
    assert MyMath.fact(1) == 1
  end

  test "fact of 5 is 120" do
    assert MyMath.fact(5) == 120
  end

  test "fact of 10 is 120" do
    assert MyMath.fact(10) == 3628800
  end

  test "fact of 10 is 120 (using tail recursion)" do
    assert MyMath.fact(10) == 3628800
  end
end
