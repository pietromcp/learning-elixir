defmodule MyTupleTest do
  use ExUnit.Case
  doctest MyTuple

  test "access tuple elements" do
    assert MyTuple.first({"x", 1, :aa}) == "x"
  end

  test "tuple decomposition" do
  	{x, y} = {:hello, 19}
  	assert x == :hello
  	assert y == 19
  end
end
