person = "Pietro"
IO.puts("Hello, #{person}!")

defmodule Hello do
	def hello(to) do
		"Hello, #{to}!"
	end
end

IO.puts(Hello.hello("World"))
IO.puts(Hello.hello("People"))

myHello = fn(to) -> "Hello, #{to}!!!" end
IO.puts(myHello.("CodicePlastico"))

people = ["Pietro", "Madda"] ++ ["Davide", "Claudio"]
Enum.each(Enum.map(people, fn x -> Hello.hello(x) end), fn msg -> IO.puts(msg) end)
Enum.each(Enum.map(people, &Hello.hello/1), fn msg -> IO.puts(msg) end)

defmodule MyEnum do	
	def tee list, action do
		Enum.map(list, fn item -> 
			action.(item)
			item
		end)
	end
end

people = ["Qui", "Quo", "Qua"]
people
	|> Enum.map(&String.upcase/1)
	|> Enum.map(&Hello.hello/1)
	|> MyEnum.tee(&IO.puts/1)
	|> Enum.map(&String.upcase/1)
	|> Enum.each(&IO.puts/1)
