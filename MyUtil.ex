defmodule MyUtil do
	def applyf(l, f) when is_list(l) do
		IO.puts "[#{Enum.join(l, ", ")}] --> #{f.(l)}"
	end
	def applyf(n, f) do
		IO.puts "#{n} --> #{f.(n)}"
	end
end
