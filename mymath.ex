import MyUtil

defmodule MyMath do
	def fact(0) do
		1
	end

	def fact(n) when n > 0 do
		n * fact(n - 1)
	end

	defp fact_loop(acc, 0) do
		acc
	end

	defp fact_loop(acc, n) when n > 0 do
		fact_loop(acc * n, n - 1)
	end

	def fact_tr(n) do
		fact_loop(1, n)
	end
end

tryWith = fn n ->
	IO.puts "#{n} --> #{MyMath.fact(n)}"
end

tryWith.(0)
tryWith.(1)
tryWith.(5)
tryWith.(10)

applyf(10, &MyMath.fact/1)
applyf(10, &MyMath.fact_tr/1)